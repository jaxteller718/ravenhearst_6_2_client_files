﻿using System;
using Harmony;
using System.Reflection;
using UnityEngine;
using DMT;

public class RH_ShowCraftingAreaAndToolInRecipeDescription
{
    public class Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_CraftingInfoWindow))]
    [HarmonyPatch("GetBindingValue")]
    [HarmonyPatch(new Type[] { typeof(string), typeof(BindingItem) }, new ArgumentType[] { ArgumentType.Ref, ArgumentType.Normal })]
    class PatchXUiC_CraftingInfoWindowGetBindingValue
    {
        static void Postfix(XUiC_CraftingInfoWindow __instance, ref bool __result, ref string value, ref BindingItem binding)
        {
            string fieldName = binding.FieldName;
            if (fieldName != null && fieldName == "itemdescription")
            {
                string text = string.Empty;
                if (__instance.recipe != null)
                {
                    ItemClass forId = ItemClass.GetForId(__instance.recipe.itemValueType);
                    if (forId != null)
                    {
                        if (forId.IsBlock())
                        {
                            string descriptionKey = Block.list[__instance.recipe.itemValueType].DescriptionKey;
                            if (Localization.Exists(descriptionKey, string.Empty))
                            {
                                text = Localization.Get(descriptionKey, string.Empty);
                            }
                        }
                        else
                        {
                            string itemDescriptionKey = forId.GetItemDescriptionKey();
                            if (Localization.Exists(itemDescriptionKey, string.Empty))
                            {
                                text = Localization.Get(itemDescriptionKey, string.Empty);
                            }
                        }
                        if (!string.IsNullOrEmpty(__instance.recipe.craftingArea))
                        {
                            text = string.Format("{0} ({1} : {2})", text, Localization.Get("CraftingArea", string.Empty), Localization.Get(__instance.recipe.craftingArea, string.Empty));
                        }
                        if (__instance.recipe.craftingToolType > 0)
                        {
                            ItemClass itemClass = ItemClass.list[__instance.recipe.craftingToolType];
                            if (itemClass != null)
                            {
                                text = string.Format("{0} ({1} : {2})", text, Localization.Get("Tool", string.Empty), itemClass.GetLocalizedItemName());
                            }
                        }
                        if (forId.IsBlock())
                        {
                            //Log.Out("XUiC_CraftingInfoWindow classname : " + forId.GetItemName());
                            var block = Block.list[__instance.recipe.itemValueType];

                            if(block != null)
                            {
                                //Log.Out("XUiC_CraftingInfoWindow blockname : " + block.GetBlockName());
                                var lootListId = 0;
                                if(block is BlockLoot)
                                {
                                    var blockLoot = block as BlockLoot;
                                    lootListId = blockLoot.lootList;

                                }
                                else if(block is BlockSecureLoot)
                                {
                                    var blockSecureLoot = block as BlockSecureLoot;
                                    lootListId = blockSecureLoot.lootList;
                                }
                                //Log.Out("XUiC_CraftingInfoWindow lootListId : " + lootListId);

                                if (lootListId > 0 )
                                {
                                    if(LootContainer.lootList[lootListId].size != null)
                                        text = string.Format("{0} ({1} : {2})", text, Localization.Get("Slots", string.Empty), LootContainer.lootList[lootListId].size.x * LootContainer.lootList[lootListId].size.y);
                                }
                            }
                        }
                    }
                }
                value = text;
                __result = true;
            }
        }
    }
}
