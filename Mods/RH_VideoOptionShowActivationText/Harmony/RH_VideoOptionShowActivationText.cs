﻿using System;
using Harmony;
using System.Reflection;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Linq;
using UnityEngine;
using DMT;

public class RH_VideoOptionShowActivationText
{
    public class RH_VideoOptionShowActivationTextInit : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_OptionsVideo))]
    [HarmonyPatch("Init")]
    class PatchXUiC_OptionsVideoInit
    {
        static void Postfix(XUiC_OptionsVideo __instance)
        {
            __instance.comboShowActivationText = __instance.GetChildById("ShowActivationText").GetChildByType<XUiC_ComboBoxBool>();
            __instance.comboShowActivationText.OnValueChangedGeneric += __instance.AnyPresetValueChanged;
        }
    }

    [HarmonyPatch(typeof(XUiC_OptionsVideo))]
    [HarmonyPatch("applyChanges")]
    public class PatchXUiC_OptionsVideoapplyChanges
    {
        static bool Prefix(XUiC_OptionsVideo __instance)
        {
            GamePrefs.Set(EnumGamePrefs.OptionsShowActivationText, (bool)__instance.comboShowActivationText.Value);

            return true;
        }
    }

    [HarmonyPatch(typeof(XUiC_OptionsVideo))]
    [HarmonyPatch("updateGraphicOptions")]
    public class PatchXUiC_OptionsVideoupdateGraphicOptions
    {
        static bool Prefix(XUiC_OptionsVideo __instance)
        {
            __instance.comboShowActivationText.Value = GamePrefs.GetBool(EnumGamePrefs.OptionsShowActivationText);

            return true;
        }
    }

    [HarmonyPatch(typeof(NGUIWindowManager))]
    [HarmonyPatch("SetLabelText")]
    [HarmonyPatch(new Type[] { typeof(EnumNGUIWindow), typeof(string), typeof(bool) })]
    public class PatchNGUIWindowManagerSetLabelText
    {
        public static bool Prefix(NGUIWindowManager __instance, ref EnumNGUIWindow _eElement, ref string _text, ref bool _bLocalize)
        {
            if (_eElement == EnumNGUIWindow.ActivateInGame && !GamePrefs.GetBool(EnumGamePrefs.OptionsShowActivationText))
            {
                return false;
            }

            return true;
        }
    }
}
