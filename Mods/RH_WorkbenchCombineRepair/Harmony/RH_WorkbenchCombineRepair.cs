﻿using System;
using Harmony;
using System.Reflection;
using UnityEngine;
using DMT;

public class RH_WorkbenchCombineRepair
{
    public class Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_CombineGrid))]
    [HarmonyPatch("Merge_SlotChangedEvent")]
    [HarmonyPatch(new Type[] { typeof(int), typeof(ItemStack) })]
    class PatchXUiC_CombineGridMerge_SlotChangedEvent
    {
        static bool Prefix(XUiC_CombineGrid __instance, ref int slotNumber, ref ItemStack stack)
        {
            if (__instance.merge1.ItemStack.IsEmpty() || __instance.merge2.ItemStack.IsEmpty() || __instance.merge1.ItemStack.itemValue.type != __instance.merge2.ItemStack.itemValue.type || __instance.merge1.ItemStack.itemValue.UseTimes == __instance.merge1.ItemStack.itemValue.MaxUseTimes || __instance.merge2.ItemStack.itemValue.UseTimes == __instance.merge2.ItemStack.itemValue.MaxUseTimes)
            {
                __instance.result1.SlotChangedEvent -= __instance.Result1_SlotChangedEvent;
                __instance.result1.ItemStack = ItemStack.Empty.Clone();
                __instance.result1.HiddenLock = true;
                __instance.result1.SlotChangedEvent += __instance.Result1_SlotChangedEvent;
                return false;
            }
            if (__instance.merge1.ItemStack.itemValue.type == __instance.merge2.ItemStack.itemValue.type)
            {
                ItemStack itemStack1 = __instance.merge1.ItemStack;
                ItemStack itemStack2 = __instance.merge2.ItemStack;
                ItemStack itemStack3 = __instance.merge1.ItemStack.Clone();

                if (itemStack1.itemValue.Quality <= 2 && itemStack2.itemValue.Quality <= 2)
                    return false;

                bool hasMods = false;       
                foreach (var mod in itemStack1.itemValue.Modifications)
                {
                    if (mod != null && mod.IsMod)
                        hasMods = true;
                }
                foreach (var mod in itemStack2.itemValue.Modifications)
                {
                    if (mod != null && mod.IsMod)
                        hasMods = true;
                }
                if (hasMods)
                {
                    GameManager.ShowTooltipWithAlert(__instance.xui.playerUI.entityPlayer, Localization.Get("workbenchCombineHasMods", Localization.QuestPrefix), "ui_denied");
                    return false;
                }


                float percent1 = (float)(itemStack1.itemValue.MaxUseTimes - itemStack1.itemValue.UseTimes) / (float)itemStack1.itemValue.MaxUseTimes;
                float percent2 = (float)(itemStack2.itemValue.MaxUseTimes - itemStack2.itemValue.UseTimes) / (float)itemStack2.itemValue.MaxUseTimes;
                float combinedPercent = Mathf.Min(percent1 + percent2, 1f);
                int newQuality = Mathf.Max(itemStack1.itemValue.Quality, itemStack2.itemValue.Quality) - 4;

                itemStack3.itemValue = new ItemValue(itemStack3.itemValue.type, newQuality, newQuality);
                itemStack3.itemValue.UseTimes = itemStack3.itemValue.MaxUseTimes - (int)(itemStack3.itemValue.MaxUseTimes * combinedPercent);

                __instance.result1.ItemStack = itemStack3;
                __instance.result1.HiddenLock = false;
                __instance.lastResult = itemStack3;
            }

            return false;
        }
    }
}
