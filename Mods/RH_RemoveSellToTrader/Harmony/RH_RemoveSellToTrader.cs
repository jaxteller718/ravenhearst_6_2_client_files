﻿using System;
using Harmony;
using System.Reflection;
using UnityEngine;
using DMT;

public class RH_RemoveSellToTrader
{
    public class Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiM_Trader))]
    [HarmonyPatch("GetSellPrice")]
    [HarmonyPatch(new Type[] { typeof(XUi), typeof(ItemValue), typeof(int), typeof(ItemClass) })]
    class PatchXUiM_TraderGetSellPrice
    {
        static bool Prefix(XUiM_Trader __instance, ref int __result, ref XUi _xui, ref ItemValue itemValue, ref int count, ref ItemClass itemClass)
        {
            if(itemClass.GetItemName().StartsWith("mod"))
            {
                __result = 0;
                return false;
            }

            return true;
        }
    }
}
