﻿using System;
using Harmony;
using System.Reflection;
using UnityEngine;
using DMT;

public class RH_ReverseRotation
{
    public class Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(BlockToolSelection))]
    [HarmonyPatch("ExecuteAttackAction")]
    [HarmonyPatch(new Type[] { typeof(ItemInventoryData), typeof(bool), typeof(PlayerActionsLocal) })]
    class PatchBlockToolSelectionExecuteAttackAction
    {
        static bool Prefix(BlockToolSelection __instance, ref bool __result, ItemInventoryData _data, ref bool _bReleased, ref PlayerActionsLocal playerActions)
        {
            if (!_bReleased)
            {
                __result = false;
                return false;
            }
            bool flag = false;
            if (GameManager.Instance.IsEditMode() && playerActions.Drop.IsPressed)
            {
                __result = false;
                return false;
            }
            if (!playerActions.SelectionSet.IsPressed && __instance.SelectionActive)
            {
                if (!playerActions.Drop.IsPressed)
                {
                    flag = (flag || __instance.SelectionActive);
                    if (__instance.SelectionLockMode == 1)
                    {
                        Vector3i selectionSize = __instance.SelectionSize;
                        __instance.SelectionStart = _data.hitInfo.hit.blockPos;
                        __instance.SelectionEnd = __instance.SelectionStart + selectionSize - Vector3i.one;
                    }
                    else if (__instance.SelectionLockMode == 0)
                    {
                        __instance.SelectionActive = false;
                    }
                }
            }
            else if (GameManager.Instance.IsEditMode() && playerActions.Run.IsPressed && _data.hitInfo.bHitValid)
            {
                Vector3i blockPos = playerActions.Run.IsPressed ? _data.hitInfo.hit.blockPos : _data.hitInfo.lastBlockPos;
                __instance.setBlock(_data.hitInfo.hit.clrIdx, blockPos, BlockValue.Air);
                flag = true;
            }
            else if (_data is ItemClassBlock.ItemBlockInventoryData)
            {
                ItemClassBlock.ItemBlockInventoryData itemBlockInventoryData = (ItemClassBlock.ItemBlockInventoryData)_data;
                if (itemBlockInventoryData.mode == BlockPlacement.EnumRotationMode.Auto)
                {
                    itemBlockInventoryData.mode = BlockPlacement.EnumRotationMode.Simple;
                }
                BlockValue bv = itemBlockInventoryData.itemValue.ToBlockValue();
                bv.rotation = itemBlockInventoryData.rotation;
                bv = Block.list[bv.type].BlockPlacementHelper.OnPlaceBlock(itemBlockInventoryData.mode, itemBlockInventoryData.localRot, _data.world, bv, _data.hitInfo.hit, itemBlockInventoryData.holdingEntity.position).blockValue;
                byte rotation = itemBlockInventoryData.rotation;
                itemBlockInventoryData.rotation = Block.list[itemBlockInventoryData.itemValue.ToBlockValue().type].BlockPlacementHelper.LimitRotation(itemBlockInventoryData.mode, ref itemBlockInventoryData.localRot, _data.hitInfo.hit, !playerActions.Run.IsPressed, bv, bv.rotation);
                flag = true;
                if (rotation != itemBlockInventoryData.rotation)
                {
                    itemBlockInventoryData.holdingEntity.PlayOneShot("rotateblock", false);
                }
            }
         
            __result = flag;
            return false;
        }
    }
}
